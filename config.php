<?php

//  initiating script parameters

    $servername = "localhost";
    $username   = "user";
    $password   = "password";
    $dbname     = "wunder";
    $tableName  = "customer";

    $customerFields = array(
        "firstName"     ,
        "lastName"      ,
        "phoneNumber"   ,
        "streetAddress" ,
        "houseNumber"   ,
        "zipCode"       ,
        "city"          ,
        "owner"         ,
        "iban"          ,
    );

?>

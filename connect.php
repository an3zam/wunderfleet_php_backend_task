 <?php

class dbConnector{
      
    private $conn = null;
    public function openConnection($servername,$dbname,$username,$password) {
        try {
            $this->conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
        }catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function closeConnection() {
        $this->conn=null;
    }
   
    public function insert($tableName,$params){

        $customer = array();
        // generating sql insert request
        $keys=""; 
        $vals="";
        foreach($params as $key => $val){
            $keys        .= "`$key`,";
            $vals        .= ":$key,";
        }
        $keys = rtrim($keys, ',');
        $vals = rtrim($vals, ',');
        $sql = "INSERT INTO customer ( $keys ) VALUES ( $vals )";
        ($this->conn)->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = ($this->conn)->prepare($sql);
        $stmt->execute($params);
    }

    public function select($tableName, $selectedColumns, $condition, $conditionArray){
        ($this->conn)->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $cols = ''; foreach( $selectedColumns as $col) { $cols.= "$col,"; $cols = rtrim($cols, ',');}
        $stmt = ($this->conn)->prepare("SELECT $cols from $tableName where $condition"); 
        $stmt->execute($conditionArray);
        $stmt->setFetchMode(PDO::FETCH_ASSOC); 
        if($resultArray = $stmt->fetch()) { return $resultArray; }else{ return array(); }
    }


    public function update($tableName, $selectedColumns, $condition, $conditionArray){
            try{                
                $cols = ''; foreach( $selectedColumns as $col) { $cols.= "$col = :$col,"; $cols = rtrim($cols, ',');}
                ($this->conn)->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql =  "UPDATE $tableName set $cols where $condition";
                $stmt = ($this->conn)->prepare($sql); 
                $stmt->execute($conditionArray);
                echo $conditionArray['paymentDataId'];
            }catch(Exception $e){ echo $e->getMessage(); print_r($conditionArray); print_r($sql); }
    }

}
?> 

// document structure 
var structure = {
        'personal information' : [
                        ['text','firstName', 'FirstName'],['text','lastName','LastName'],['tel','phoneNumber','Telephone']
        ],
        'address information' : [
                        ['text','streetAddress','Address including street'],['number','houseNumber','house number'],['text','zipCode','zip code'],['text','city','city code']
        ],
        'payment information' : [
                        ['text','owner','Account owner'],['text','iban','IBAN']
        ]
}



// the form controls all the views
function Form(){
	this.object	= document.createElement('div');
	this.viewList = [];
	this.addView = function(view){ this.viewList.push(view);this.object.appendChild(view.object); }
        // check if the form is valide
        this.isValide   = function (){          
		    var isValide=true; var i=0;
		    while(i<this.viewtList.length){ isValide=(isValide && this.viewList[i].isValide()); i++; }
		    console.log(isValide);
		    return isValide; }
        // for next use step=1; for previous we use step=-1
    this.changeView = function(step){
                // shows an error message and stops prevents the validation of the current view
        if(!this.getCurrentView().isValide() && step==1){
            document.getElementById('error').className=''; return;
        }
                // hides the error message
            document.getElementById('error').className='invisible';

             // if it's the currentView  is valide
            if(this.viewList.indexOf(this.getCurrentView()) == this.viewList.length-1 && step==1){   // if it's the last form
                this.submit(); 
                // show the success page
            }else{
                this.getCurrentView().save();
                this.getCurrentView().hide();
                this.setCurrentView(Math.max(0,(this.viewList.indexOf(this.getCurrentView())+step)));
                this.getCurrentView().show();
            }
    }

    // localStorage
    this.load               = function(){ for(view of this.viewList){ view.load(); } this.getCurrentView(); };
    this.save               = function(){ for(view of this.viewList){ view.save(); } };

    this.getCurrentView     = function(){ return this.viewList[(localStorage.getItem('currentView') || 0)]; };
    this.setCurrentView     = function(viewIndex){ localStorage.setItem('currentView',viewIndex); };
    this.show               = function(){
          this.load();
          this.getCurrentView().show();   
    }
    // generates the query to be submited via Ajax 
    this.getQuery = function(){ 
        var query = ''; for( view of  this.viewList){ query += '&'+view.getQuery();}; console.log(query); return query;
    }
    // to prevent multiple submission
    this.submited = false;
    this.submit = function(){
        if(this.isValide() && !this.submited){
            this.submited = true;
      //  if(query != 'error'){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                           console.log(this.responseText);
                           document.getElementById("formView").className = "invisible";
                           document.getElementById("successView").className = "";
                        if(this.responseText=='error'){   
                           document.getElementById("message").innerHTML = this.responseText;
                        }else{
                           document.getElementById("message").innerHTML  = this.responseText;
                           document.getElementById("fullName").innerHTML = localStorage.getItem('firstName')+" "+localStorage.getItem('lastName');
                           localStorage.clear();
                        }
                    }
                };
                xmlhttp.open("GET", "submit.php?"+this.getQuery(), true);
                xmlhttp.send();
            }
    //    }
    };

	this.isValide	= function(){
        for(view of  this.viewList){ 
            if(!view.isValide()){ return false; } 
       } return true;
    };
}
// a view contains a group of inputs
function View(parentForm,title){
    this.parent = parentForm;
    this.object    = document.createElement('div');
// append Title
	this.viewTitle = document.createElement('div');
    this.viewTitle.className = "view-title";
    this.viewTitle.innerHTML = title;
    this.object.appendChild(this.viewTitle);

// append view inputs
    this.viewContent = document.createElement('div');
    this.viewContent.className = "view-content";
    this.object.appendChild(this.viewContent);

	this.inputList	= [];
	this.addInput   = function (input){ 
        this.inputList.push(input);
         this.viewContent.appendChild(input.object);
 }
	this.isValide	= function(){
        for(input of  this.inputList){ 
            if(!input.isValide()){  return false; } 
       } return true;
    };
    this.save = function(){ for(input of  this.inputList){ input.save(); } };
    this.load = function(){ for(input of  this.inputList){ input.load();} };

	this.hide       = function() { this.object.className = 'invisible'; };
    this.show       = function() { this.object.className = '';  };
    this.getQuery = function(){ 
        var query = ''; for( input of  this.inputList){ query += '&'+input.getQuery();}; return query;
    }
    this.hide();
}

// A controller for input objects
function Input(type,name,placeholder){
	this.object = document.createElement('input');
	this.object.name = name;
	this.object.type = type;
	this.object.placeholder = placeholder;
// setters
    this.setValue   = function(value){ this.object.value = value || ''; }
    this.setName   = function(name){ this.object.name = name; }
// getters
    this.getValue   = function(){ return this.object.value; }
    this.getName    = function(){ return this.object.name; }
    this.isValide   = function(){ return this.getValue() != ''; }
    this.save       = function(){ localStorage.setItem(this.getName(),this.getValue());  }
    this.load       = function(){ this.setValue(localStorage.getItem(this.getName())); }
    this.getQuery = function(){ 
        return  this.getName()+'='+this.getValue();
    }
}
// element creation
var mainForm = new Form();
// generate views and inputs corresponding to the structure object 
for (var viewTitle in  structure) {
    var inputList = structure[viewTitle];
	var view = new View(mainForm,viewTitle);
    for (var element of inputList) {
	view.addInput(new Input(element[0],element[1],element[2]));
    }
	//view.createHTML();
	mainForm.addView(view);
}
//mainForm.createHTML();
function loadForm(){ 
    document.getElementById("welcomeView").className="invisible";
    document.getElementById("formView").className="";
    document.getElementById("container").appendChild(mainForm.object);
    mainForm.show();
}
// ignoring any locally saved information
function loadNewForm(){
    localStorage.clear();
    loadForm();
}
// moves to the next view
function nextForm(){
    mainForm.changeView(1);
 }

// returns to the previous view
function previousForm(){
    mainForm.changeView(-1);
 }

// check the existence of an incomplete regitration process
function checkLocalStorage(){
    if(localStorage.getItem('currentView')){
        document.getElementById("loadOldButton").className="";
    }
}


<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);
    require('connect.php');
    require('verify.php');
    require('config.php');

    //verify that all fields are submited from client
    $customer = array();
   foreach($customerFields as $field){
      if(isset($_GET[$field]) && ($_GET[$field]!='')){
	      $customer["$field"] = $_GET[$field];
        }else{
           echo "field $field is missing"; exit(0); 
        }    
    }


    // initiate connection to wunder database in localhost
    $pdo = new dbConnector();
    $pdo->openConnection($servername,$dbname,$username,$password);
//    $pdo->connect($servername,$dbname,$username,$password);
    $pdo->insert($tableName,$customer);

    $result = $pdo->select($tableName,array("customerId"),"iban=:iban",array('iban' => $customer['iban']));

    if(isset($result['customerId'])){ $customer['customerId']=$result['customerId']; }else{  echo "can't find this customer in database"; }

    $paymentDataId = getPaymentDataId(
        array(
        'customerId' => $customer['customerId'],
        "iban" => $customer['iban'],
        "owner" => $customer['owner'])
    );

    // updating PaymentDataId for the current customer based on the customerId
    if($paymentDataId == 'error'){ 
        
        echo "can't retrieve this paymentDataId of this customer";
    }else{
        $pdo->update($tableName,array("paymentDataId"),"customerId = :customerId",array(
                'customerId'    => $customer['customerId'],
                'paymentDataId' => $paymentDataId)
        );
    }


 ?>

<?php

    // verify this client with wunder API server and get back the paymentDataId
    function getPaymentDataId($table){

        //var_dump($table);

        $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
        $ch = curl_init($url);

        $params = http_build_query($table);

        $params = '{
            "customerId": 1,
            "iban": "DE8234",
            "owner": "Max Mustermann"
        }';
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
        $response = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($response,true);
        if(isset($json['paymentDataId'])){
          return $json['paymentDataId']; 
        }else{ return 'error'; }
    }

?>
